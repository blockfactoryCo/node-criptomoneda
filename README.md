NODE [NODE]
Tribus Algo PoW/PoS Hybrid Cryptocurrency

Intro

NODE is a true optional anonymous, untraceable, and secure hybrid cryptocurrency.

Ticker: NODE

Node [NODE] is an anonymous, untraceable, energy efficient, Proof-of-Work (New Tribus Algorithm) and Proof-of-Stake cryptocurrency. 1,000,000,000 NODE will be created in approx. about 3 years during the PoW phase. 

Specifications
Total number of coins: 1,000,000,000 NODE
Ideal block time: 30 seconds
Stake interest: 6% annual static inflation
Confirmations: 10 blocks
Maturity: 30 blocks (15 minutes)
Min stake age: 8 hours
Cost of Hybrid Fortuna Stakes: 66,666DE
Hybrid Fortuna Stake Reward: 33% of the current block reward

Technology

Hybrid PoW/PoS Fortuna Stakes
Stealth addresses
Ring Signatures (16 Recommended)
Native Optional Tor Onion Node (-nativetor=1)
Encrypted Messaging
Multi-Signature Addresses & TXs
Atomic Swaps using UTXOs (BIP65 CLTV)
BIP39 Support (Coin Type 114)
Proof of Data (Image/Data Timestamping)
Fast 30 Second Block Times
New/First Tribus PoW Algorithm comprising of 3 NIST5 algorithms
Tribus PoW/PoS Hybrid
Full decentralization